import React, { useState, useEffect } from 'react';
import {View, Text, StyleSheet} from 'react-native';

import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth';

import TabNavigator from './src/components/TabNavigator';
import StackNavigator from './src/components/StackNavigator';
import Login from './src/screens/stack-navigation/Login';

export default function App() {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber;
  }, []);

  // console.log(user);

  if (!user) {
    return <StackNavigator />;
  }
  if (initializing) return null;

  return <TabNavigator />;
}
