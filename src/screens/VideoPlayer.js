import React from 'react';
import Video from 'react-native-video';
import {StyleSheet} from 'react-native';

export default function VideoPlayer() {
  return (
    <Video
      source={{uri: 'http://193.37.213.144:8080/hls/stream.m3u8'}} // Can be a URL or a localfile.
      ref={ref => {
        this.player = ref;
      }} // Store reference
      onBuffer={this.onBuffer} // Callback when remote video is buffering
      onEnd={this.onEnd} // Callback when playback finishes
      onError={this.videoError} // Callback when video cannot be loaded
      style={styles.backgroundVideo}
      controls={true}
    />
  );
}

const styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
