import React from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import WebView from 'react-native-webview';

export default function Archive() {
  return (
    <SafeAreaView style={styles.flexContainer}>
      <WebView source={{uri: 'http://tv1channel.org/play/index.php'}} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  flexContainer: {
    flex: 1,
  },
});
