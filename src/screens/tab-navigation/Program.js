import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import WebView from 'react-native-webview';

export default function Program() {
  return (
    <View style={styles.container}>
      <Text>Program</Text>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});
