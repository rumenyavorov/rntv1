import React, {Component, useState, useEffect} from 'react';
import {
  SafeAreaView,
  Text,
  StyleSheet,
  View,
  TouchableNativeFeedback,
  Modal,
  Image,
} from 'react-native';
import Video from 'react-native-video';
import auth from '@react-native-firebase/auth';
import Button from '../../components/Button';
import Header from '../../components/Header';
import {useNavigation} from '@react-navigation/native';

const logOut = () => {
  auth()
    .signOut()
    .then(() => console.log('User signed out!'));
};

export default function Live() {
  const navigation = useNavigation();

  return (
    <View style={styles.screen}>
      <View style={styles.screen}>
      </View>
      <View style={styles.imageContainer}>
        <Image
          source={require('../../../images/logo.png')}
          style={styles.image}
        />
      </View>
      <View style={styles.buttons}>
        <Button
          title="TV 1 Live"
          onPress={() => navigation.navigate('VideoPlayer')}
        />
        <Button title="TV 1 Audio" />
        <Button title="Logout" onPress={() => logOut()} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    alignItems: 'center',
  },
  imageContainer: {
    position: 'absolute',
  },
  image: {
    width: 420,
    height: 400,
  },
  screen: {
    flex: 1,
  },
  buttons: {
    flex: 1,
    top: 100,
    alignItems: 'center',
  },
});
