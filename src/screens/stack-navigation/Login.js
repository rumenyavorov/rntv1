import React, {useState, Component} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import auth, {FirebaseAuthTypes} from '@react-native-firebase/auth';
import Button from '../../components/Button';

export default function Login({navigation}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handlePress = () => {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        console.log('User signed successfully');
        navigation.navigate('Login');
      })
      .catch(error => {
        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }
        console.error(error);
      });
  };

  return (
    <View style={styles.screen}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>login</Text>
      </View>
      <View style={styles.textFieldContainer}>
        <TextInput
          style={styles.inputField}
          placeholder="Email"
          placeholderTextColor="#808080"
          onChangeText={input => setEmail(input)}
          autoCapitalize="none"
        />
        <TextInput
          style={styles.inputField}
          placeholder="Password"
          placeholderTextColor="#808080"
          onChangeText={input => setPassword(input)}
          secureTextEntry={true}
          autoCapitalize="none"
        />
      </View>
      <View style={styles.buttonContainer}>
        <Button
          title="Login"
          onPress={() => handlePress()}
          style={styles.loginButton}
        />
        <Button
          title="Cancel"
          onPress={() => navigation.goBack()}
          style={styles.cancelButton}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: 'center',
  },
  headerContainer: {
    width: '95%',
    height: 55,
    top: 120,
    backgroundColor: '#31BAE4',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
  },
  headerText: {
    color: 'white',
    fontSize: 30,
    fontWeight: '300',
  },
  textFieldContainer: {
    width: '90%',
    maxWidth: '90%',
    top: '20%',
  },
  buttonContainer: {
    flexDirection: 'row',
    top: '50%',
  },
  buttonText: {
    color: 'white',
    fontSize: 22,
  },
  loginButton: {
    backgroundColor: '#31BAE4',
    paddingVertical: 15,
    borderRadius: 30,
    width: 200,
    alignItems: 'center',
    margin: 5,
  },
  cancelButton: {
    backgroundColor: 'crimson',
    paddingVertical: 15,
    borderRadius: 30,
    width: 200,
    alignItems: 'center',
    margin: 5,
  },
  inputField: {
    borderWidth: 1,
    borderRadius: 30,
    borderColor: '#31BAE4',
    padding: 15,
    textAlign: 'center',
    margin: 3,
    color: 'black',
  },
});
