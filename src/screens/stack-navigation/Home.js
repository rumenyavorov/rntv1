import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import Button from '../../components/Button';

export default function Home({navigation}) {
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          source={require('../../../images/logo.png')}
          style={styles.image}
        />
      </View>
      <View style={styles.buttons}>
        <Button
          title="Go to Login"
          onPress={() => navigation.navigate('Login')}
        />
        <Button
          title="Go to Register"
          onPress={() => navigation.navigate('Register')}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttons: {
    top: 250,
  },
  imageContainer: {
    position: 'absolute',
    top: -1,
  },
  image: {
    width: 420,
    height: 400,
  },
});
