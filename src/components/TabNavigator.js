import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import News from '../screens/tab-navigation/News';
import Live from '../screens/tab-navigation/Live';
import Program from '../screens/tab-navigation/Program';
import Archive from '../screens/tab-navigation/Archive';
import AboutUs from '../screens/tab-navigation/AboutUs';
import VideoPlayer from '../screens/VideoPlayer';

const Tab = createBottomTabNavigator();

export default function TabNavigator() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({color, size}) => {
            let iconName;
            
            if (route.name === 'Live') {
              iconName = 'ios-play';
            } else if (route.name === 'News') {
              iconName = 'ios-newspaper';
            } else if (route.name === 'Program') {
              iconName = 'ios-list';
            } else if (route.name === 'Archive') {
              iconName = 'ios-archive';
            } else if (route.name === 'About us') {
              iconName = 'ios-information-circle';
            }

            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'blue',
          inactiveTintColor: 'gray',
        }}>
        <Tab.Screen name="Live" component={Live} />
        <Tab.Screen name="News" component={News} />
        <Tab.Screen name="Program" component={Program} />
        <Tab.Screen name="Archive" component={Archive} />
        <Tab.Screen name="About us" component={AboutUs} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
