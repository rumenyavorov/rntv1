import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const Button = props => {
  return (
    <TouchableOpacity onPress={props.onPress}>
      <View style={{...styles.button, ...props.style}}>
        <Text style={{...styles.buttonText, ...props.textStyling}}>
          {props.title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#31BAE4',
    paddingVertical: 20,
    paddingHorizontal: 25,
    borderRadius: 30,
    width: 250,
    alignItems: 'center',
    margin: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 22,
  },
});

export default Button;
